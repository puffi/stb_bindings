mkdir -p libs
mkdir -p .build
cd .build
gcc ../stbc/stb_truetype.c -c
ar rcs libstb_truetype.a stb_truetype.o
cp libstb_truetype.a ../libs
